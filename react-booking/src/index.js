import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css'

//let element = <h1>Hello World!</h1>

//console.log(element);


/*
  ReactDOM.render() - allows to render/display our reactjs elements and display 
  in our html.

  ReactDOM.render(<reactElement>,<htmlElementsSelectedById>)

  */



  /*let myName = (
  <>
    <h2>Adrian Fontanilla</h2>
    <h3>Full Stack Developer</h3>
  </>
  )*/

//let name = "Jeffrey Lacdao";
//let job = "Professional Driver";

/*let personDisplay1 = (
  <>
    <h1>{name}</h1>
    <h2>{job}</h2>
  </>
)*/

//let num1 = 150;
//let num2 = 250;

/*let numDisplay = (

  <>
    <h4>The sum of {num1} and {num2} is:</h4>
    <p>{num1+num2}</p>
  </>
)*/

/*let person = {

  name: "Stephen Strange",
  age:45,
  job: "Sorcerer Supreme",
  income: 50000,
  expense: 30000,
  address: "New York City, New York"
}*/

/*let personDisplay2 = <p>Hi! My name is {person.name}. I am {person.age}
  years old. I live in {person.address}</p>*/


/*let personDisplay3 = (

  <>
    <h1>{person.name}</h1>
    <h2>{person.job}</h2>
    <h3>{person.age} years old.</h3>
    <h3>{person.address}</h3>
    <h4>{person.income}</h4>
    <h4>{person.expense}</h4>
    <h4>{person.income - person.expense}</h4>
  </>

)*/

/* 
ReactJS components are parts of the page. Independent UI parts
of our app.

Components are functions that return react elements.

Components naming convention: PascalCase

Components should start in capital letters.


*/

/*const SampleComp = () => {

  return (

      <h1>I am a react element returned by a function.</h1>

    )
}*/





ReactDOM.render(
  <React.StrictMode>
  <App />
</React.StrictMode>,
  document.getElementById('root')
);





























