import{Row, Col} from 'react-bootstrap'


export default function About(){

		return (

			<Row className="bg-secondary">
				
				<Col className="p-5">
					
					<h1 className="my-5">About Me</h1>
					<h2 className="mt-3">Adrian F.</h2>
					<h3>Full Stack Web Developer.</h3>
						<p className="mb-4">
							Adrian is a very hardworking person who is versatile with 
							developing front-end and back-end applications.

						</p>
					<h4>Contact Me</h4>
						<ul>
							<li>Adrian@gmail.com</li>
							<li>09664973245</li>
							<li>Makati, Manila</li>


						</ul>

				</Col>

			</Row>

	)

}