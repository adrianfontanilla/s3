

//react-bootstrap components are react components that create/return react
//elements with the appropriate bootstrap classes.
import{Button, Row, Col} from 'react-bootstrap'


export default function Banner(){

	return (
		
		<Row>
			<Col className="p-5">
				<h1 className="mb-3">B152 Booking System</h1>
				<p className="my-3">Opportunities for everyone, everywhere!</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
			
		</Row>
		)


}