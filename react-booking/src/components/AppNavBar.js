import{Nav,Navbar,Container} from 'react-bootstrap'

export default function AppNavBar(){

	return (

			<Navbar bg="primary" expand="lg">
				<Container fluid>
					<Navbar.Brand href="#home">B152 Booking</Navbar.Brand>
					<Navbar.Toggle aria-controls="basic-navbar-nav" />
					<Navbar.Collapse id="basic-navbar-nav">
						<Nav className="ml-auto">
							<Nav.Link href="#home">Home</Nav.Link>
							<Nav.Link href="#courses">Courses</Nav.Link>
						</Nav>
					</Navbar.Collapse>
				</Container>
				
			</Navbar>


		)


}