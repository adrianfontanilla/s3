//import react as a package before making react component.
//import is similar to require() in expressjs.
//import React from 'react';


//components shouldbe able to return something or react elements.
//if you want to return a blank page return null

import {Container} from 'react-bootstrap';

import Banner from './components/Banner'
import AppNavBar from './components/AppNavBar'
import About from './components/About'


export default function App(){

  return (
      <>
        <AppNavBar />
        <Container>
          <Banner />
          <About />
        </Container>
      </> 
        
    )
}
